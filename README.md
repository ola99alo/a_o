# Libancall APP API

## Prerequisists:
- Docker
- Docker Compose
- VSCode (optional but recommended)

## Setup:
- Clone the repo to your local machine:
```bash
git clone git@gitlab.com:libancallprojects/libancallapp/apis.git
```
- Run the containerized dev environment:
```bash
docker-compose up -d
```
- Within your VSCode console, run the migrations:
```bash
./manage.py migrate
```
- Create a django super user:
```bash
./manage.py createsuperuesr # follow the instructions
```
- Run the django app:
```bash
./manage.py runserver 0:8000
```

## Development:


## Deployment
