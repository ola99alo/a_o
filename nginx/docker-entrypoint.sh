#!/usr/bin/env bash

# Exit the script as soon as something fails.
set -e

PLACEHOLDER_STATIC_ROOT="/API/src"
PLACEHOLDER_BACKEND_NAME="api_api"
PLACEHOLDER_BACKEND_PORT="8000"


# http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html
if [ "$LOCAL_TEST" = "1" ]; then
    echo "Setting up local test mode"
    PLACEHOLDER_BACKEND_NAME="api"
    PLACEHOLDER_VHOST="localhost"
else
    echo "Starting up production instance"
    PLACEHOLDER_VHOST="$(curl http://169.254.169.254/latest/meta-data/hostname)"
fi

# Where is our default config located?
DEFAULT_CONFIG_PATH="/etc/nginx/conf.d/default.conf"

# Replace all instances of the placeholders with the values above.
sed -i "s:PLACEHOLDER_VHOST:${PLACEHOLDER_VHOST}:g" "${DEFAULT_CONFIG_PATH}"
sed -i "s:PLACEHOLDER_STATIC_ROOT:${PLACEHOLDER_STATIC_ROOT}:g" "${DEFAULT_CONFIG_PATH}"
sed -i "s:PLACEHOLDER_BACKEND_NAME:${PLACEHOLDER_BACKEND_NAME}:g" "${DEFAULT_CONFIG_PATH}"
sed -i "s:PLACEHOLDER_BACKEND_PORT:${PLACEHOLDER_BACKEND_PORT}:g" "${DEFAULT_CONFIG_PATH}"

# Execute the CMD from the Dockerfile and pass in all of its arguments.
exec "$@"
