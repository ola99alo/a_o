FROM python:3.10.1-buster

LABEL libancallapp.project="api"

# Add PostgreSQL repo so we can install the client for version 12
RUN echo "deb http://apt.postgresql.org/pub/repos/apt buster-pgdg main" \
        > /etc/apt/sources.list.d/pgdg.list \
    && curl -s https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

ARG PRODUCTION

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        bash-completion \
        dirmngr \
        gdal-bin \
        gettext \
        gnupg \
        jq \
        libxmlsec1-dev \
        openssh-client \
        postgresql-client-12 \
    && pip install --upgrade pip \
    && pip install pipenv --no-cache-dir \
    && rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/vishnubob/wait-for-it.git /wait-for-it

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - 

ENV API_ROOT /apis/src
RUN mkdir -p $API_ROOT

WORKDIR $API_ROOT

COPY src/Pipfile src/Pipfile.lock ${API_ROOT}/

ARG DEBUG

RUN set -x && \
    if [ "$DEBUG" = "1" ]; then \
        (unset DEBUG; pipenv install --clear --system -d); \
    else \
        (unset DEBUG; pipenv install --clear --system); \
    fi

ARG USER_UID
ARG USER_GID

RUN set -x  \
    && if [ ! $(getent group ${USER_GID:-${USER_UID:-1000}}) ]; then \
        groupadd libancallapp --gid=${USER_GID:-${USER_UID:-1000}}; \
    fi \
    && useradd -d /home/libancallapp -m --gid=${USER_GID:-${USER_UID:-1000}} --uid=${USER_UID:-1000} libancallapp

COPY src $API_ROOT/
RUN chown -R ${USER_UID:-1000}:${USER_GID:-${USER_UID:-1000}} $API_ROOT

COPY baseimage_init /sbin/my_init
RUN chmod +x /sbin/my_init

USER libancallapp
RUN mkdir -p /home/libancallapp/.vscode-server/data/Machine

# Collect the static files
ENV PYTHONUNBUFFERED=1

# Create a volume for the static files
VOLUME ["$API_ROOT/static"]

# CMD [ \
#     "/sbin/my_init", \
#     "--skip-startup-files", \
#     "--skip-runit", \
#     "--", \
#     "wsgi:application", \
#     "--capture-output", \
#     "--access-logfile",  "-", \
#     "--access-logformat", "%({X-REAL-IP}i)s %(l)s %(u)s %(t)s \"%(r)s\" %(s)s %(b)s \"%(f)s\" \"%(a)s", \
#     "--error-logfile", "-", \
#     "--workers", "2", \
#     "--timeout", "60", \
#     "--bind", "0.0.0.0:8000" \
# ]

ENTRYPOINT ["tail", "-f", "/dev/null"]
