from .base_settings import *  # noqa

DEBUG = True

CORS_ORIGIN_REGEX_WHITELIST = (
    r'^(https?://)?192\.168\.\d+.\d+(:\d+)?$',
)

ALLOWED_HOSTS = ['*']

# Setting value to 0, will set this variable to None, leading to no truncation.
SHELL_PLUS_PRINT_SQL_TRUNCATE = (
    ENV.int('SHELL_PLUS_PRINT_SQL_TRUNCATE', default=1000) or  # noqa: F405
    None
)

# EMAIL_BACKEND = 'apps.common.utils.SandboxEmailBackend'
