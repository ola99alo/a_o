from .base_settings import *

TESTING_MODE = True

REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = (  # pylint:diabable=F405
    'rest_framework.authentication.SessionAuthentication',
)
